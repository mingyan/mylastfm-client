'use strict';

/* https://github.com/angular/protractor/blob/master/docs/toc.md */

describe('my app', function() {


  it('should automatically redirect to /index when location hash/fragment is empty', function() {
    browser.get('index.html');
    expect(browser.getLocationAbsUrl()).toMatch("/index");
  });


  describe('default', function() {

    beforeEach(function() {
      browser.get('index.html#/index');
    });


    it('should render default when user navigates to /index', function() {
      expect(element.all(by.css('h1')).first().getText()).
        toContain('Last.fm');
    });

  });


  describe('search', function() {

    beforeEach(function() {
      browser.get('index.html#/search/australia/1');
    });


    it('should render search result when user navigates to /search/australia/1', function() {
      expect(element(by.id('summary')).getText()).
        toContain('artists found');
    });

  });
});
