'use strict';

// Declare app level module which depends on views, and components
angular.module('mylastfm', [
  'ui.router'
]).
config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/index');

    $stateProvider

      // Default STATES AND NESTED VIEWS ========================================
      .state('default', {
        url: '/index',
        templateUrl: 'views/default/partial-default.html'
      })
      .state('artist', {
        url: '/artist/:artistName/:page',
        templateUrl: 'views/artist/partial-artist.html',
        controller: function ($scope, $stateParams, $http){
          var artistName = $stateParams.artistName;
          var page = parseInt($stateParams.page);
          console.log('Artist name:' + artistName);
          $scope.artistName = artistName;
          $scope.isFirstPage = false;
          $scope.isLastPage = false;

          if (page <= 1) {
            page = 1;
            $scope.isFirstPage = true;
          }
          //@TODO, It is not DRY. Refactor it to use factory or service
          $http.get('http://ws.audioscrobbler.com/2.0/?method=artist.getTopTracks&api_key=1f13dae823f02f8d045f0622c861fe9f&format=json&limit=5&artist=' + artistName + '&page=' + page)
          //$http.get('http://localhost:3000/api/get-top-tracks?artist=' + artistName + '&page=' + page)
          .then(function(response){

              if (response.data) {
                if (response.data.error) {
                  //Better have a proper error message modal, ngmodal?
                  alert(response.data.message);
                  return;
                }
                console.log(response);

                $scope.topTracks = response.data.toptracks.track;
                $scope.metaInfo = response.data.toptracks['@attr'];

                var currentPage = parseInt(response.data.toptracks['@attr'].page);
                var totalPages = parseInt(response.data.toptracks['@attr'].totalPages);
                if (currentPage >= totalPages) {
                  currentPage = totalPages;
                  $scope.isLastPage = true;
                }

                //generate the page numbers
                if ((currentPage - 5) <= 1 ) {
                  $scope.previousPageNumbers = range(1, currentPage - 1);
                } else {
                  $scope.previousPageNumbers = range(currentPage - 5, currentPage - 1);
                }

                if ((currentPage + 5) >=  totalPages) {
                  $scope.nextPageNumbers = range(currentPage + 1, totalPages);
                } else {
                  $scope.nextPageNumbers = range(currentPage + 1, currentPage + 5);
                }

                $scope.currentPage = currentPage;

              }
            });

        }
      })

      // Result PAGE AND MULTIPLE NAMED VIEWS =================================
      .state('search', {
        url:'/search/:country/:page',
        templateUrl: 'views/search/partial-search.html',
        controller: function ($scope, $stateParams, $http, $rootScope) {
          var country = $stateParams.country;
          var page = parseInt($stateParams.page);

          console.log('Get country: ' + country);
          console.log('Get page: ' + page);
          $rootScope.country = country;
          //$scope.country = country;
          $scope.isFirstPage = false;
          $scope.isLastPage = false;

          if (page <= 1) {
            page = 1;
            $scope.isFirstPage = true;
          }

          //@TODO, refactor it to use factory or service
          $http.get('http://ws.audioscrobbler.com/2.0/?method=geo.getTopArtists&api_key=1f13dae823f02f8d045f0622c861fe9f&format=json&limit=5&country=' + country + '&page=' + page)
          //$http.get('http://localhost:3000/api/get-top-artists?country=' + country + '&page=' + page)
            .then(function(response){

              if (response.data) {
                if (response.data.error) {
                  alert(response.data.message);
                  return;
                }
                console.log(response);
                $scope.topArtists = response.data.topartists.artist;
                $scope.metaInfo = response.data.topartists['@attr'];

                var currentPage = parseInt(response.data.topartists['@attr'].page);
                var totalPages = parseInt(response.data.topartists['@attr'].totalPages);
                if (currentPage >= totalPages) {
                  currentPage = totalPages;
                  $scope.isLastPage = true;
                }

                //generate the page numbers
                if ((currentPage - 5) <= 1 ) {
                  $scope.previousPageNumbers = range(1, currentPage - 1);
                } else {
                  $scope.previousPageNumbers = range(currentPage - 5, currentPage - 1);
                }

                if ((currentPage + 5) >=  totalPages) {
                  $scope.nextPageNumbers = range(currentPage + 1, totalPages);
                } else {
                  $scope.nextPageNumbers = range(currentPage + 1, currentPage + 5);
                }

                $scope.currentPage = currentPage;

              }
            });




        }
      });
  })
  .controller('IndexController', IndexController);

function IndexController($scope, $state, $rootScope) {
  $scope.topArtists = [];


  $scope.search = function (country) {
    console.log('Country is ' + country);
    $rootScope.country = $scope.country;
    //Go to the search state
    $state.go('search', {country: country, page: 1})

  }
}

//Helper function to get the array for ng-repeat
var range = function(min, max) {

  var input = [];
  for (var i = min; i <= max; i++) {
    input.push(i);
  }
  return input;
};


